package development.test.touchmobiletest.model

import java.io.Serializable

data class Store(
    val address: String,
    val city: String,
    val name: String,
    val latitude: Double,
    val zipcode: String,
    val storeLogoURL: String,
    val phone: String,
    val longitude: Double,
    val storeID: String,
    val state: String
): Serializable