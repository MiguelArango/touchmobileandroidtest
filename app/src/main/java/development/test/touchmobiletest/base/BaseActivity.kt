package development.test.touchmobiletest.base

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity

abstract class BaseActivity: AppCompatActivity(), BaseView {

    protected abstract fun getLayoutId(): Int
    protected abstract fun onSetupView(intent: Intent)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        onSetupView(intent)


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    override fun showError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
    }

    fun Context?.isAvailable(): Boolean {
        if (this == null) {
            return false
        } else if (this !is Application) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (this is FragmentActivity) {
                    return !this.isDestroyed
                } else if (this is Activity) {
                    return !this.isDestroyed
                }
            }
        }
        return true
    }

    override fun context(): Context {
        return this
    }
}