package development.test.touchmobiletest.base

import android.content.Context

interface BaseView {


    fun showLoading()
    fun dismissLoading()
    fun showError(errorMessage: String)

    fun context(): Context
}