package development.test.touchmobiletest.repo

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import development.test.touchmobiletest.api.BottlerockApiImpl
import development.test.touchmobiletest.api.response.GetStoresResponse
import development.test.touchmobiletest.callback.GetListCallback
import development.test.touchmobiletest.model.Store
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class StoreRepoImpl(val context: Context) : StoreRepo {

    private val TAG = "StoreRepo"

    override fun getList(callback: GetListCallback) {
        if(isOnline()){
            val api = BottlerockApiImpl()
            val responseCall: Call<GetStoresResponse> = api.getStores()
            responseCall.enqueue(object : Callback<GetStoresResponse> {
                override fun onResponse(call: Call<GetStoresResponse>, response: Response<GetStoresResponse>) {
                    if (response.isSuccessful) {
                        callback.onGetListSuccess(response.body()?.stores as ArrayList<Store>)
                    }   else{
                        callback.onGetListError(response.errorBody().toString())
                    }
                }
                override fun onFailure(call: Call<GetStoresResponse>, t: Throwable) {
                    callback.onGetListError(t.message.toString())
                }
            })
        }   else{
            callback.onGetListError("No tienes conexión a internet")
        }

    }

    fun isOnline():Boolean{
        val connectivityManager= context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected
    }
}