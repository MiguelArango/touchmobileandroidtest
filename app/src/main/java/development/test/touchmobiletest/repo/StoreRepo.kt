package development.test.touchmobiletest.repo

import development.test.touchmobiletest.callback.GetListCallback

interface StoreRepo {


    fun getList(callback: GetListCallback)

}