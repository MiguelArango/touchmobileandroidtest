package development.test.touchmobiletest.callback

import development.test.touchmobiletest.model.Store

interface GetListCallback {

    fun onGetListSuccess(stores: ArrayList<Store>)
    fun onGetListError(errorMessage: String)
}