package development.test.touchmobiletest.api

import development.test.touchmobiletest.api.response.GetStoresResponse
import retrofit2.Call

class BottlerockApiImpl : BottlerockApi {


    override fun getStores(): Call<GetStoresResponse> {
        val api = ApiConnection().connectionApi().requestSyncCall()
        return api.getStores()
    }
}