package development.test.touchmobiletest.api

import development.test.touchmobiletest.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit

class ApiConnection(): Callable<BottlerockApi>{

    private lateinit var api: BottlerockApi
    private lateinit var retrofit: Retrofit

    override fun call(): BottlerockApi {
        return requestSyncCall()
    }

    fun connectionApi(): ApiConnection {
        return ApiConnection()
    }

    fun connectToApi() {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
            .readTimeout(60000, TimeUnit.MILLISECONDS)
            .connectTimeout(60000, TimeUnit.MILLISECONDS)
            .addInterceptor(logging)
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .client(httpClient)
            .build()
    }

    internal fun requestSyncCall(): BottlerockApi {
        connectToApi()
        api = retrofit.create(BottlerockApi::class.java)
        return api
    }
}