package development.test.touchmobiletest.api

import development.test.touchmobiletest.BuildConfig
import development.test.touchmobiletest.api.response.GetStoresResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface BottlerockApi {

    @GET(BuildConfig.API_BASE_URL + GET_LIST_ENDPOINT)
    fun getStores(): Call<GetStoresResponse>


    companion object{
        const val GET_LIST_ENDPOINT = "BR_Android_CodingExam_2015_Server/stores.json"
    }

}