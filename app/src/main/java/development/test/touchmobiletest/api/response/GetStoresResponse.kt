package development.test.touchmobiletest.api.response

import development.test.touchmobiletest.model.Store

data class GetStoresResponse(
    val stores: List<Store>
)