package development.test.touchmobiletest.activities.stores

import development.test.touchmobiletest.callback.GetListCallback
import development.test.touchmobiletest.repo.StoreRepoImpl

class StoresInteractor(private val storeRepoImpl: StoreRepoImpl) {

    fun getStores(callback: GetListCallback){
        storeRepoImpl.getList(callback)
    }


}