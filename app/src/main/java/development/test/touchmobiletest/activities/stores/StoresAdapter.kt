package development.test.touchmobiletest.activities.stores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import development.test.touchmobiletest.R
import development.test.touchmobiletest.activities.detail.DetailItemActivity
import development.test.touchmobiletest.model.Store
import kotlinx.android.synthetic.main.item_store.view.*

class StoresAdapter (var context: Context, var stores: ArrayList<Store>): RecyclerView.Adapter<StoresAdapter.ViewHolder>() {

    private val TAG = "StoresAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_store, parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return stores.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(stores[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        lateinit var currentStore: Store
        private val tilName = itemView.tilName
        private val tilCity = itemView.tilCity
        private val tilPhone = itemView.tilPhone
        private val ivImage = itemView.ivImage
        private val tilAddress = itemView.tilAddress

        init {


            itemView.setOnClickListener {

                context.startActivity(DetailItemActivity.newIntent(context, currentStore))
            }

        }

        fun bind(store: Store){
            currentStore = store

            tilName.text = currentStore.name
            tilCity.text = currentStore.city
            tilPhone.text = currentStore.phone
            tilAddress.text = currentStore.address

            Glide.with(context).load(currentStore.storeLogoURL).into(ivImage)

        }
    }

}

