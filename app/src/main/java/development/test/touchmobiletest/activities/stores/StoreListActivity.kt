package development.test.touchmobiletest.activities.stores

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import development.test.touchmobiletest.R
import development.test.touchmobiletest.base.BaseActivity
import development.test.touchmobiletest.model.Store
import kotlinx.android.synthetic.main.activity_list_items.*

class StoreListActivity : BaseActivity(),
    StoresView {

    lateinit var presenter: StoresPresenter
    lateinit var storesAdapter: StoresAdapter
    private var currentStores =  ArrayList<Store>()

    override fun getLayoutId(): Int {
        return R.layout.activity_list_items
    }

    override fun onSetupView(intent: Intent) {
        initViewComponents()
        initPresenter()
    }

    fun initPresenter(){
        presenter =
            StoresPresenter()
        presenter.setView(this)
        presenter.initialize()
    }

    fun initViewComponents(){
        errorView.visibility = View.GONE
        storesLoader.visibility = View.GONE
        contentView.visibility = View.GONE

        errorView.setOnClickListener {
            presenter.getStores()
        }

        storesAdapter =
            StoresAdapter(
                this,
                currentStores
            )
        val dataLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        rvStores.apply {
            adapter = storesAdapter
            layoutManager = dataLayoutManager
        }
    }

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, StoreListActivity::class.java)
            return intent
        }
    }

    override fun renderStores(stores: ArrayList<Store>) {
        currentStores.clear()
        contentView.visibility = View.VISIBLE

        currentStores.addAll(stores)

        storesAdapter.notifyDataSetChanged()
    }


    override fun showLoading() {
        errorView.visibility = View.GONE
        contentView.visibility = View.GONE
        storesLoader.visibility = View.VISIBLE
    }

    override fun dismissLoading() {
        storesLoader.visibility = View.GONE
    }

    override fun showError(errorMessage: String) {
        errorView.visibility = View.VISIBLE
        tvErrorMessage.text = ""
        tvErrorMessage.text = errorMessage
    }
}
