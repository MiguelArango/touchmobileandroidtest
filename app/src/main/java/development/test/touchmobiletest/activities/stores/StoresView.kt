package development.test.touchmobiletest.activities.stores

import development.test.touchmobiletest.base.BaseView
import development.test.touchmobiletest.model.Store

interface StoresView: BaseView {

    fun renderStores(stores: ArrayList<Store>)
}