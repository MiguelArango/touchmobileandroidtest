package development.test.touchmobiletest.activities.detail

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import development.test.touchmobiletest.R
import development.test.touchmobiletest.activities.stores.StoreListActivity
import development.test.touchmobiletest.base.BaseActivity
import development.test.touchmobiletest.model.Store
import kotlinx.android.synthetic.main.activity_detail_item.*

class DetailItemActivity : BaseActivity() {
    override fun getLayoutId(): Int {
        return R.layout.activity_detail_item
    }

    override fun onSetupView(intent: Intent) {
        initViewComponents()
    }

    fun initViewComponents(){

        val store: Store = intent.getSerializableExtra(INTENT_STORE) as Store

        Glide.with(this).load(store.storeLogoURL).into(ivLogo)

        tvName.text = store.name
        tilCity.text = store.city

        ivBack.setOnClickListener {
            onBackPressed()
        }

    }

    companion object {
        const val INTENT_STORE = "store"
        fun newIntent(context: Context, store: Store): Intent {
            val intent = Intent(context, DetailItemActivity::class.java)
            intent.putExtra(INTENT_STORE, store)
            return intent
        }
    }

    override fun showLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun dismissLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
