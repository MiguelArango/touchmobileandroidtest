package development.test.touchmobiletest.activities.stores

import development.test.touchmobiletest.base.Presenter
import development.test.touchmobiletest.callback.GetListCallback
import development.test.touchmobiletest.model.Store
import development.test.touchmobiletest.repo.StoreRepoImpl

class StoresPresenter : Presenter<StoresView>(), GetListCallback {

    private lateinit var interactor: StoresInteractor

    override fun initialize() {
        interactor =
            StoresInteractor(
                StoreRepoImpl(
                    getView()?.context()!!
                )
            )

        getStores()
    }

    fun getStores(){
        getView()?.showLoading()
        interactor.getStores(this)
    }


    override fun onGetListSuccess(stores: ArrayList<Store>) {
        getView()?.dismissLoading()
        getView()?.renderStores(stores)
    }

    override fun onGetListError(errorMessage: String) {
        getView()?.dismissLoading()
        getView()?.showError(errorMessage)
    }
}